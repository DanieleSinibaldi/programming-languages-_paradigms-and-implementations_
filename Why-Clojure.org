#+STARTUP: showeverything
#+INCLUDE: "config.org"

#+OPTIONS: reveal_klipsify_src:t ^:{}
#+REVEAL_MIN_SCALE: 1.0
#+REVEAL_MAX_SCALE: 1.0

# Some optional settings for CodeMirror.
#+REVEAL_CODEMIRROR_CONFIG: codemirror_options_in: {
#+REVEAL_CODEMIRROR_CONFIG:   lineNumbers: true,
#+REVEAL_CODEMIRROR_CONFIG:   autoCloseBrackets: true
#+REVEAL_CODEMIRROR_CONFIG: }

#+TITLE: Why Clojure ?
#+AUTHOR: Bernard Hugueney
#+EMAIL: icsl@bernard-hugueney.org
#+OPTIONS: toc:nil
#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:nil reveal_control:t
#+OPTIONS: reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil

* Meta
   #+BEGIN_leftcol
[[https://gitlab.com/bhugueney/programming-languages-_paradigms-and-implementations_/blob/master/Why-Clojure.org][Link to source of this presentation]] that [[https://gitlab.com/bhugueney/programming-languages-_paradigms-and-implementations_/edit/master/Why-Clojure.org][you can edit]] (you might need to [[https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project][request access]])

[[https://gitlab.com/bhugueney/programming-languages-_paradigms-and-implementations_/issues][Link to post requests]]

[[https://bhugueney.gitlab.io/programming-languages-_paradigms-and-implementations_/Why-Clojure.html?print-pdf][Link to printable version]]
   #+END_leftcol

   #+BEGIN_rightcol
https://huit.re/oBRCrgtt

#+REVEAL_HTML: <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABjAQAAAACnQIM4AAAA6ElEQVQ4jc3UsQ3DIBAF0LMoKLOAJdZIx0pmARMWsFeiYw1LLGA6CuTLEVmyG3xWiihUvALdPw4BeF7wx1oBbCjWQ8cpYRk1ukAbTr43uTwCmBsaNRW5KTXfEVJkdUrWFPVnQm+ObpuilXJMxw02tUo6oV6yWE6bFK+nwD3LpXQNMsq+44QZJyqS1cyJzlkfVxkdKxArLIOMiVPKMOiFxttx2iTOOSLuWS6UPE2Vigjk9JmDSCgcpzpbL1zgRe9lkLXIzKq+61OyS42wdBnsDZmgJq0SKyw19ZGsKeqvjsuj4/Trn+g7vQG1rbLt01PkhAAAAABJRU5ErkJggg=="  height=400 width=400 >
   #+END_rightcol

#+BEGIN_NOTES
Add speaker notes everywhere ! ☺
#+END_NOTES



* Sapir Whorf hypothesis
(Yet) Another language ?
 - some languages make better programs
 - some also make better programmers !
* Low barrier to productivity …
thanks to perfect Java interoperability !
 - Can use/create any java object
 - Same primitive types (incl. java.lang.String)
Solves the chicken-egg problem (libraries, users base) that have been
keeping us with C for decades despite all the good work in academia.
* A Simple Language
 - Simple :: ≠ easy
 - Fighting incidental complexity :: There is no pride to have in
      mastering needlessly complex tools (java programmers understand
      this very well when talking about C++ ☺).

* Object Oriented ‽
 - type
 - identity
 - state
   - attribute names → values
   - mutability
 - (single) dynamic dispatching
 - namespace
Clojure has it *all*, /à la carte/.

Toolbox > swiss army knife
* Sane defaults
 - immutability : Value Oriented Programming (transients available but
   the persistent data structures are extremely efficient)
 - dynamic typing (type annotations also available)
 - lexical binding (dynamic binding also available)
 - static dispatching (dynamic dispatching available, single or
   multiple)
 - open structures as maps of interned strings (closed struct available)
* Practical SMP primitives
 - vars :: thread local writes
 - agents :: queued asynch writes
 - inter-threads synch :: atomic writes
   - on single /atoms/
   - transactions on multiple /refs/
* Very Simple Syntax ☺
"Oh, btw, it's a LISP"
[[file:img/lisp-angry-meme.png]]
* Don't panic !
Not necessarily /easy/ at first but :
 - syntactic grouping of pairs without ()
 - code is data (list), but most data structures are not lists :
   - [vector]
   - {map}
   - #{set}
 - IDE (e.g. emacs) to the rescue !
   - help writing (electric parenthesis)
   - help reading (rainbow parenthesis)
   - e.g. ≠ i.e. ☺
* M-x emacs-ftw
[[file:img/clojure-in-emacs.png]]
* Eclipse plug-in
[[file:img/sc-ccw.png]]
* Cursive based on IntelliJ
[[file:img/IntelliJ-Cursive.png]]

* VS Code

[[file:img/Visual-Studio.png]]

(Cf. [[https://gist.github.com/bpringe/4f1d07f98633a956a8b33af572e7b810][getting started with Clojure on Windows™]])

* Metaprogramming done right
 - David Wheeler :: "All problems in computer science can be solved by
                    another level of indirection".
 - Myself :: "Any boring (part of a) task can and should be automated away"
 - Myself again :: "Programming should be fun !"

Hence you should be able to automate boring parts of programming !
To make it /easy/, you *need* the /simple/ syntax of code as data structure.
(disclosure: I'm a [[http://www.boost.org/doc/libs/1_49_0/libs/mpl/doc/index.html][Boost::mpl]] user !)
[ [[http://blog.fogus.me/2011/11/15/the-macronomicon-slides/][Macronomicon by M.Fogus]]]

* Growing a language
Macros not only for syntactic sugar :
- [[https://github.com/clojure/core.match/wiki/Overview][Pattern matching]]
- [[http://www.slideshare.net/normanrichards/corelogic-introduction][Logic programming]]
- static typing http://logaan.github.io/clojure/core.typed/2013/10/02/core.typed-game-of-life.html
- \dots

* Conclusion
Excellent Plateform for :
 - Programming Snobs :: you can add your monads with macros !
 - Hurried / Productive Coders :: you can reuse all the java libs/frameworks !
 - Corporate Drones :: it's all jars to you, JarJar !

Not all side effects are evil : by learning this language, you'll also
learn concepts form the great minds behind it. You will also get a
better understanding of concepts you already (think you) know
(e.g. OOP, state, time, value, hammocks…).

* TL;DR
 - Don't fear/dismiss the unknown
 - Learn things and have fun ! (I know you will ☺)

"+others will never resolve on passing higher order functions in
forests of parenthesis+" ☹


I avoided LISP for 10 years :
"I was blind, now I can see.
Rich made a believer out of me !" ☺
* BTW, it now runs on the client
ClojureScript [[http://app.klipse.tech/][compiles as efficient Javascript]], [[https://clojurescript.org/reference/source-maps][with SourceMap]] .

* [Web|Bib]liography

 - Talks (slides / videos)
   - Are We There Yet ? *← Must See !*  [[https://github.com/matthiasn/talk-transcripts/blob/master/Hickey_Rich/AreWeThereYet.md][slides]] / [[http://www.infoq.com/presentations/Are-We-There-Yet-Rich-Hickey][video]]
   - [[https://github.com/matthiasn/talk-transcripts/blob/master/Hickey_Rich/HammockDrivenDev.md][Hammock Driven Development]]
   - [[https://github.com/matthiasn/talk-transcripts/blob/master/Hickey_Rich/SimpleMadeEasy.md][Simple Made Easy]]
   - [[http://vimeo.com/68334908][ClojureScript: Lisp's Revenge]]
 - Books
   - [[http://joyofclojure.com/][The Joy of Clojure]]
   - [[http://www.manning.com/rathore/][Clojure in Action]]
   - [[http://www.clojurebook.com/][Clojure Programming]]
 - Quick refs
   - [[http://www.cis.upenn.edu/~matuszek/Concise%2520Guides/Concise%2520Clojure.html][Concise Clojure]]
   - [[https://clojure.org/api/cheatsheet]]
 - Practice Material
   - [[https://exercism.io/tracks/clojure][Exercism]]

cheat sheets
* Bonus track : snippets
** FizzBuzz
#+begin_src clojure export: code
(def fizzbuzz
"lazy seq of fizzbuzz"
  (lazy-seq (map #(let [s (str (if (= 0 (rem % 3)) "Fizz")
                               (if (= 0 (rem % 5)) "Buzz"))]
                    (if (empty? s) % s))
                 (iterate inc 1))))
(take 16 fizzbuzz)
#+end_src
* Fibonacci
#+begin_src clojure export: code
(def fib-seq
  "lazy seq of Fibonacci numbers"
  (lazy-cat [0 1] (map + (rest fib-seq) fib-seq)))
(take 16 fib-seq)
#+end_src

* TODO Hands-on !
 - [[https://repl.it/languages/clojure][try clojure !]]

Remember : a proper IDE will help *a lot* you with the () !

# Local Variables:
# org-src-preserve-indentation: t
# End:
