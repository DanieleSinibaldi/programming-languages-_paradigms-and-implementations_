#+STARTUP: showeverything
#+INCLUDE: "config.org"

#+OPTIONS: reveal_klipsify_src:t ^:{}
#+REVEAL_MIN_SCALE: 1.0
#+REVEAL_MAX_SCALE: 1.0

# Some optional settings for CodeMirror.
#+REVEAL_CODEMIRROR_CONFIG: codemirror_options_in: {
#+REVEAL_CODEMIRROR_CONFIG:   lineNumbers: true,
#+REVEAL_CODEMIRROR_CONFIG:   autoCloseBrackets: true
#+REVEAL_CODEMIRROR_CONFIG: }

#+TITLE: Parsing
#+AUTHOR: Bernard Hugueney
#+EMAIL: icsl@bernard-hugueney.org
#+OPTIONS: toc:nil
#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:nil reveal_control:t
#+OPTIONS: reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil

#+PROPERTY: mkdirp yes
#+BEGIN_SRC elisp :exports none :results silent
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t) (shell . t) (latex . t) (lisp . t)))
#+END_SRC

* Meta
   #+BEGIN_leftcol
[[https://gitlab.com/bhugueney/programming-languages-_paradigms-and-implementations_/blob/master/Lecture-03.org][Link to source of this presentation]] that [[https://gitlab.com/bhugueney/programming-languages-_paradigms-and-implementations_/edit/master/Lecture-03.org][you can edit]] (you might need to [[https://docs.gitlab.com/ee/user/project/members/#request-access-to-a-project][request access]])

[[https://gitlab.com/bhugueney/programming-languages-_paradigms-and-implementations_/issues][Link to post requests]]

[[https://bhugueney.gitlab.io/programming-languages-_paradigms-and-implementations_/Lecture-03.html?print-pdf][Link to printable version]]
   #+END_leftcol
   #+BEGIN_rightcol
URL: https://huit.re/bnkAof28
#+REVEAL_HTML: <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAABjAQAAAACnQIM4AAAA6klEQVQ4jc3UsQ3DIBAF0EMUdPECSF4jnVcyCxgzgVeiYw0kFjAdBfLlnERyGvtQiihUvOLEPw4B+Lngj7UC2BA7D4JTxjoNtQu04eS1KVUEMA2aBt21CpqE1freHclORf2ZoM3R7alo5RLNcYOnWhVV9LOqlhMG3O4S31mutIEW1KLSglP2aaZDSr+wotvyaVXJccKS5j17ypwywkhbGi+n7Z4WHy2+slwpY7wpuYJETs85yIzScaLZjiBd4EXvZRz2uoXV/q73UmzQBFEUsA0aFdyGPrOiLEDjOpKd6dlfNB4dp1//RN/pAWU3rWXVN0KnAAAAAElFTkSuQmCC"  height=400 width=400 >
   #+END_rightcol
#+BEGIN_NOTES
Add speaker notes everywhere ! ☺
#+END_NOTES


* Programs made of text

text \rightarrow in-memory data structure


Language : infinite combinatorial possibilities of statements \rightarrow
recursive generative structure (*grammar* rules)

* What is reading ?
Characters \rightarrow words \rightarrow sentences


Symbols \rightarrow tokens \rightarrow statements
** Lexing
characters \rightarrow words
*** Regular expression
Regular Languages can be parsed by finite automata.
- union
- concatenation
- Kleene Star

over empty language and signletons of the alphabet.

** Parsing
words \rightarrow sentences

Regular expressions cannot express /structure/ (e.g. matching opening and
closing tags).

*** Context Free grammars
Well-formed (balanced) parentheses :
- S \rightarrow SS
- S \rightarrow (S)
- S \rightarrow ()
* Ambiguities & Lookahead
** Greed
** Ordering

* Don't parse unless you have to !
Use existing languages and parsers :
** Data

- JSON
- XML


** Code
Homoiconicity !

* Hands on
- install and use the [[https://github.com/Engelberg/instaparse][instaparse Clojure library]]
- try online on [[http://instaparse-live.matt.is/][live instaparse on Clojurescript]]

** Comma separated sequence of numbers

*** positive integers

*** positive and negative integers

*** integers or floats

** S-expressions
- ()
- ( name )
- ( number )
- ( name number name …)
- ( name () …)
- …

name start with a letter and contain letters or digits. 
** Arithmetic expressions
- additions and substractions of numbers
- additions, substractions, multiplications and divisions of numbers
- additions, substractions, multiplications and divisions of numbers,
  grouping with parentheses
** Solutions
We want to allow whitespaces but we don't want them in the result
We use commas for parsing but don't need it in the result

[[http://instaparse-live.matt.is/#/-LrkYRaU_DhBhtpubNsc/v1][See solution on intaparse-live]]

#+ATTR_REVEAL: :no-klipsify t
#+BEGIN_SRC clojure
"SEQ= <SPACES>? NUMBER (<SPACES>? <','> <SPACES>? NUMBER)* <SPACES>?
NUMBER= #'-?[0-9]+'
SPACES= #'\\s+'"
#+END_SRC

#+REVEAL: split

We use a NUMBER rule to factor things out, but do not need it to
appear in the result. The content of this rule should appear, so =<>=
on the rule name in the definition, not use.

[[http://instaparse-live.matt.is/#/-LrkZMaAi4GM9ctj_bIM/v1][See solution on intaparse-live]]
#+ATTR_REVEAL: :no-klipsify t
#+BEGIN_SRC clojure
"SEQ= <SPACES>? NUMBER (<SPACES>? <','> <SPACES>? NUMBER)* <SPACES>?
<NUMBER>= INTEGER | FLOAT
INTEGER= #'-?[0-9]+'
FLOAT= #'-?[0-9]+\\.[0-9]*'
SPACES= #'\\s+'"
#+END_SRC

#+REVEAL: split
S-expression

[[http://instaparse-live.matt.is/#/-Lrk_5f5rUfHzzBr3OOF/v1][See solution on instaparse-live]]

#+ATTR_REVEAL: :no-klipsify t
#+BEGIN_SRC clojure
"SEXP= <SPACES>? <'('> <SPACES>? (VALUE (<SPACES> VALUE)*)? <SPACES>? <')'> <SPACES>?
VALUE= NUMBER | NAME | SEXP
<NUMBER>= INTEGER | FLOAT
INTEGER= #'-?[0-9]+'
FLOAT= #'-?[0-9]+\\.[0-9]*'
NAME= #'[a-zA-Z][a-zA-Z0-9]*'
SPACES= #'\\s+'"
#+END_SRC

#+REVEAL: split

Arithmetic expressions

Recursive : left ? right ?  both ? (→ambiguities)

#+ATTR_REVEAL: :no-klipsify t
#+BEGIN_SRC clojure
"EXP= <SPACES>? FLOAT (<SPACES>('+' | '-')<SPACES> EXP <SPACES>?)? <SPACES>?
FLOAT= #'-?[0-9]+\\.[0-9]*'
SPACES= #'\\s+'"

#+END_SRC
 
#+ATTR_REVEAL: :no-klipsify t
#+BEGIN_SRC clojure
"EXP= <SPACES>?(EXP <SPACES> ('+' | '-') <SPACES> <SPACES>?)? FLOAT <SPACES>?
FLOAT= #'-?[0-9]+\\.[0-9]*'
SPACES= #'\\s+'
#+END_SRC

[[http://instaparse-live.matt.is/#/-LrkaM169EDrKOeM6JgU/v1][See solution on instaparse-live]]

#+ATTR_REVEAL: :no-klipsify t
#+BEGIN_SRC clojure
"EXP= <SPACES>?(VALUE <SPACES> ('+' | '-') <SPACES> <SPACES>?)? VALUE <SPACES>?
VALUE= EXP | FLOAT
FLOAT= #'-?[0-9]+\\.[0-9]*'
SPACES= #'\\s+'"
#+END_SRC
