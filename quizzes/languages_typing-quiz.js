quiz = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "What do you know about static vs dynamic typing?  Let's see...",
        "level1":  "Excellent!", // 80-100%
        "level2":  "Well done!", // 60-79%
        "level3":  "You may need to re-read some parts.", // 40-59%
        "level4":  "Did you just jump here?",             // 20-39%
        "level5":  "Please restart from the beginning."   // 0-19%, no comma here
    },
    "questions": [
        { // Question 1 - Multiple Choice, Single True Answer
            "q": "Which statement about static typing is correct?",
            "a": [
                {"option": "Static typing prevents compilation errors", "correct": false},
                {"option": "Static typing enables compilation errors", "correct": true},
                {"option": "Static typing is more convenient", "correct": false},
                {"option": "Static typing is most useful with interpreted languages", "correct": false} // no comma here
            ],
            "correct": "<p><span>Correct!</span> Yes, static typing enables some compilation errors, that are much better than runtime errors!</p>",
            "incorrect": "<p><span>No.</span> You may want to start over.</p>" // no comma here
        },
        { // Question 2 - Multiple Choice, Multiple True Answers, Select All
            "q": "Which languages are statically typed? (Select ALL.)",
            "a": [
                {"option": "C", "correct": true},
                {"option": "Java", "correct": true},
                {"option": "C++", "correct": true},
                {"option": "Haskell", "correct": true},
		{"option": "Agda", "correct": true},
                {"option": "Idris", "correct": true} // no comma here
            ],
            "correct": "<p><span>Correct!</span> You got it all.</p>",
            "incorrect": "<p><span>Not Quite.</span> Try again.</p>" // no comma here
        } // no comma here
    ]
};
